module.exports = {
  root: false,
  env: {
    browser: false,
    node: false,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    'comma-dangle': [ 'error', 'always-multiline' ],
    curly: [ 'error', 'multi' ],
    'no-console': 'off',
    camelcase: 'off',
    'array-bracket-spacing': 'off',
  },
}
