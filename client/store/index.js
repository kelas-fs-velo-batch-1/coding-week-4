import Cookies from 'js-cookie'
import { cookieFromRequest } from '~/utils'
 
export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    const token = cookieFromRequest(req, 'auth_token')
    if (token) {
      commit('auth/SET_TOKEN', token)
    }
 
    const locale = cookieFromRequest(req, 'locale')
    if (locale) {
      commit('lang/SET_LOCALE', { locale })
    }
  },
 
  nuxtClientInit ({ commit }) {
    var queryDict = {}
    var token
    var remember = false
    window.location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]})
    if (queryDict.token) {
      token = queryDict.token
      if (token) {
        Cookies.set('auth_token', token, { expires: remember ? 365 : null })
        console.log('loading autologin token', token)
      }
    } else {
      token = Cookies.get('auth_token')
      if (!token || token == 'undefined') {
        token = null
      }
    }
    if (queryDict.message) {
      alert(queryDict.message)
    }
 
    if (token && token != 'undefined') {
      commit('auth/SET_TOKEN', token)
      console.log('init token', token)
    }
 
  }
}
