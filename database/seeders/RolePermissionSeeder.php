<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\User;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::firstOrCreate(['name' => 'admin']);
        $user_list = Permission::firstOrCreate(['name' => 'user.list']);
        $user_create = Permission::firstOrCreate(['name' => 'user.create']);
        $user_edit = Permission::firstOrCreate(['name' => 'user.edit']);
        $user_delete = Permission::firstOrCreate(['name' => 'user.delete']);
        $course_list = Permission::firstOrCreate(['name' => 'course.list']);
        $course_create = Permission::firstOrCreate(['name' => 'course.create']);
        $course_edit = Permission::firstOrCreate(['name' => 'course.edit']);
        $course_delete = Permission::firstOrCreate(['name' => 'course.delete']);
        $admin->syncPermissions([
            $user_list,
            $user_create,
            $user_edit,
            $user_delete,
            $course_list,
            $course_create,
            $course_edit,
            $course_delete,
        ]);

        $user = User::where('username', 'kamilersz')->first();
        $user->assignRole('admin');
    }
}
